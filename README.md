# Underwater GAN (UGAN)

### [[Original Project Page]](http://irvlab.cs.umn.edu/enhancing-underwater-imagery-using-generative-adversarial-networks) [[arxiv]](https://arxiv.org/pdf/1801.04011.pdf) [[Video]](https://www.youtube.com/watch?v=tgTHleu9L0U) [[Original Code]](https://github.com/cameronfabbri/Underwater-Color-Correction)
Color correction for underwater vision. This project uses CycleGAN to perform style transfer on "clean" underwater
photos to appear to be underwater.


## Installation
- Clone this repo and create a conda environment: `conda env create -f environment.yml`



## Run
- Download the weights located [here](https://tiiuae-my.sharepoint.com/:f:/g/personal/paola_ardon_tii_ae/ErbFVtV8-9tCssrtImxzjeYBDqApsMpMu9K1YDtifNFg2g?e=YFsEff) and place them in `/checkpoints`
- Download the dataset located [here](https://drive.google.com/file/d/1LOM-2A1BSLaFjCY2EEK3DA2Lo37rNw-7/view) and place it inside `/datasets`
- To convert images run `python eval_folder.py` or for videos `python convert_video.py <path/to/video.mp4>`
